package com.mnc.bank.employeemanagement.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/rest/employee")
@RestController
public class EmployeeController {

    public void createEmployee(){

    }

    public void updateEmployee(){

    }

    public void deleteEmployee(){

    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getEmployee(){
        System.out.println("in Get");
        return "kiran";
    }

}
